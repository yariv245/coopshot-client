import React, { useState, useEffect } from "react";
import {
  Text,
  StyleSheet,
  View,
  Button,
  Image,
  ScrollView,
  Dimensions,
} from "react-native";
import { UploadFromCamera } from "../api/albums";
import {
  requestPermissionsAsync,
  hasServicesEnabledAsync,
} from "expo-location";

const w = Dimensions.get("window").width;
const h = Dimensions.get("window").height;

const ToUploadScreen = ({ navigation }) => {
  const photoObj = navigation.getParam("photo");
  const albumId = navigation.getParam("albumId");
  const isPrivate = navigation.getParam("isEnabled");

  const [errorMsg, setErrorMsg] = useState(null);

  const getLocation = async () => {
    try {
      if (!hasServicesEnabledAsync()) {
        setErrorMsg("Locatin services must be enabled");
        return;
      }

      let { status } = await requestPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("Permission to access location was denied");
        return;
      }
    } catch (err) {
      setErrorMsg(err);
    }
  };
  useEffect(() => {
    getLocation();
  }, []);

  if (errorMsg)
    return (
      <View>
        <Text>{errorMsg} </Text>
      </View>
    );

  return (
    <ScrollView>
      <View>
        <View style={styles.containerImage}>
          <Image source={{ uri: photoObj.uri }} style={styles.imageStyle} />
        </View>
        <Text style={styles.titleStyle}>
          Are you sure you want to upload this photo ?
        </Text>

        <Button
          color={"green"}
          title="Yes"
          onPress={() => {
            // Request to upload new Picture to the ServerSide With FormDate
            UploadFromCamera(photoObj, albumId, isPrivate);
            // navigation.navigate("SingleAlbum", { albumId }); // ****Need to pass here the id of the album again
            navigation.goBack();
          }}
        />
        <Button
          style={{}}
          color={"red"}
          title="No"
          onPress={() => {
            navigation.goBack();
          }}
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 18,
    marginBottom: 10,
    fontWeight: "bold",
    marginTop: 40,
    marginLeft: 10,
  },
  imageStyle: {
    height: "100%",
    width: "100%",
  },
  containerImage: {
    marginTop: 100,
    height: h - 300,
    width: w - 20,
    marginHorizontal: 10,
    borderColor: "black",
    borderWidth: 3,
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: "green",
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: "bold",
    letterSpacing: 0.25,
    color: "white",
  },
});

export default ToUploadScreen;
