import React, { useEffect, useContext, useState } from "react";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import {
  View,
  Text,
  Image,
  ImageBackground,
  TextInput,
  FlatList,
  SafeAreaView,
} from "react-native";
import {
  getCurrentPositionAsync,
  requestPermissionsAsync,
  hasServicesEnabledAsync,
} from "expo-location";
import axios from "../api/albums";
import { Context as AlbumContext } from "../context/AlbumContext";
import { Context as GlobalContext } from "../context/GlobalContext";

const HomeScreen = ({ navigation }) => {
  // const [favAlbums, setFavAlbums] = useState(null);
  const { setAllStateAlbum } = useContext(AlbumContext);
  const { state, getFavAlbums } = useContext(GlobalContext);

  useEffect(() => {
    setAllStateAlbum();
    getRecommendation();
    getFavAlbums();
  }, []);

  // const getFavAlbums123 = async () => {
  //   var allFavAlbums = await axios.get("/user/getAllFav");
  //   setFavAlbums(allFavAlbums.data);
  // };

  var favIds = [];

  const getLocation = async () => {
    try {
      let { status } = await requestPermissionsAsync();
      if (status !== "granted") {
        console.error("Permission to access location was denied");
        return;
      }

      if (!hasServicesEnabledAsync()) {
        console.error("Locatin services must be enabled");
        return;
      }

      let glocation = await getCurrentPositionAsync({});

      const longUser = glocation.coords.longitude;
      const latUser = glocation.coords.latitude;
      return { longUser, latUser };
    } catch (err) {
      console.error(err);
    }
  };

  const getRecommendation = async () => {
    try {
      const { longUser, latUser } = await getLocation();
      const tags = await axios.get("/api/recommendation", {
        params: {
          latUser: latUser,
          longUser: longUser,
        },
      });
    } catch (err) {
      console.error(err);
    }
  };

  if (state == undefined) return <View></View>;

  return (
    <ImageBackground
      source={require("../../assets/backHome.png")}
      style={{ width: "100%", height: "100%" }}
    >
      <ScrollView>
        <View
          style={{
            // flexDirection: 'row',
            marginTop: 40,
            alignItems: "space-between",
          }}
        >
          <View style={{ alignItems: "flex-end", paddingHorizontal: 15 }}>
            <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
              <Icon name="account-circle" size={33} color="white" />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ paddingHorizontal: 40, marginTop: 25 }}>
          <Text
            style={{
              fontSize: 40,
              color: "black",
            }}
          >
            Hello
          </Text>

          <Text
            style={{
              fontSize: 15,
              paddingVertical: 10,
              paddingRight: 40,
              lineHeight: 22,

              color: "black",
            }}
          >
            CoopShot is a smarter home for all your photos and videos, made for
            the way you take photos today.
          </Text>

          <TouchableOpacity
            onPress={() => navigation.navigate("Recommendation")}
            style={{
              flexDirection: "row",
              backgroundColor: "#FFF",
              borderRadius: 40,
              alignItems: "center",
              paddingVertical: 10,
              paddingHorizontal: 20,
              marginTop: 30,
            }}
          >
            <Image
              source={require("../../assets/searchHome.png")}
              style={{ height: 18, width: 14, marginTop: 2 }}
            />
            <TextInput
              placeholder="Search For Album"
              style={{ paddingHorizontal: 20, fontSize: 15, color: "#ccccef" }}
            />
          </TouchableOpacity>

          <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{ marginRight: -40, marginTop: 30 }}
          >
            <TouchableOpacity
              onPress={() => navigation.navigate("AlbumCreation")}
              style={{
                alignItems: "center",
                justifyContent: "center",
                height: 66,
                width: 66,
                borderRadius: 50,
                backgroundColor: "#5facdb",
              }}
            >
              <Icon name="plus" color="white" size={32} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => navigation.navigate("Camera")}
              style={{
                alignItems: "center",
                justifyContent: "center",
                height: 66,
                width: 66,
                borderRadius: 50,
                backgroundColor: "#ff5c83",
                marginHorizontal: 22,
              }}
            >
              <Icon name="camera" color="white" size={32} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                navigation.navigate("Albums", {
                  favIds: favIds,
                })
              }
              style={{
                alignItems: "center",
                justifyContent: "center",
                height: 66,
                width: 66,
                borderRadius: 50,
                backgroundColor: "#ffa06c",
              }}
            >
              <Ionicons name="albums" size={32} color="white" />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => navigation.navigate("Recommendation")}
              style={{
                alignItems: "center",
                justifyContent: "center",
                height: 66,
                width: 66,
                borderRadius: 50,
                backgroundColor: "#bb32fe",
                marginLeft: 22,
              }}
            >
              <MaterialIcons name="favorite" size={32} color="white" />
            </TouchableOpacity>
          </ScrollView>

          <View
            style={{
              flexDirection: "row",
            }}
          >
            <Text
              style={{
                color: "#FFF",

                marginTop: 50,
                fontSize: 30,
              }}
            >
              Favorites
            </Text>

            {/* {tagsRecommendedList != null ? (

          ) : null} */}
          </View>

          <SafeAreaView
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{ flex: 1, marginHorizontal: -30, marginTop: 30 }}
          >
            <FlatList
              data={state.favAlbums}
              horizontal={true}
              keyExtractor={(album) => album._id}
              renderItem={({ item }) => {
                return (
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate("SingleAlbum", {
                        albumId: item._id,
                      })
                    }
                  >
                    <View
                      style={{
                        backgroundColor: "#FEFEFE",
                        height: 200,
                        width: 190,
                        borderRadius: 15,
                        padding: 5,
                        margin: 5,
                      }}
                    >
                      <Image
                        source={
                          item.photos && item.photos[0]
                            ? {
                              uri: item.photos[0].url,
                            }
                            : require("../../assets/No_Image_Available.jpg")
                        }
                        style={{ width: 180, borderRadius: 10, height: 130 }}
                      />
                      <View
                        style={{
                          flexDirection: "row",
                          width: 150,
                          alignItems: "center",
                        }}
                      >
                        <View
                          style={{
                            paddingHorizontal: 5,
                            paddingVertical: 5,
                          }}
                        >
                          <Text
                            style={{
                              fontSize: 20,
                              color: "#a2a2db",
                            }}
                          >
                            {item.name.split("~")[1]}
                          </Text>
                          <Text
                            style={{
                              fontSize: 14,
                            }}
                          >
                            {item.description}
                          </Text>
                        </View>
                        <Icon
                          name="map-marker"
                          size={25}
                          color="#ff5c83"
                          style={{ marginBottom: 15 }}
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                );
              }}
            />
          </SafeAreaView>
        </View>
      </ScrollView>
    </ImageBackground>
  );
};

export default HomeScreen;
