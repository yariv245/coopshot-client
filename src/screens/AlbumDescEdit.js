import React, { useState, useContext } from "react";
import { Text, StyleSheet, View, Button, TextInput } from "react-native";
import { Context } from "../context/AlbumContext";

const AlbumDescEdit = ({ navigation }) => {
  const { state, editAlbum } = useContext(Context);
  const album = state.find((album) => album._id === navigation.getParam("id"));

  const [description, setDescription] = useState(album.description);

  const saveChanges = () => {
    editAlbum(navigation.getParam("id"), album.name, description, () =>
      navigation.navigate("SingleAlbum", { id: album.id })
    );
  };

  return (
    <View style={{ marginTop: 70 }}>
      <Text style={styles.label}>Enter New Description:</Text>
      <TextInput
        multiline={true}
        textAlignVertical={"top"}
        numberOfLines={10}
        style={styles.InputStyle}
        value={description}
        onChangeText={(text) => setDescription(text)}
      />
      <Button title="Save Changes" onPress={saveChanges} />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
  },
  InputStyle: {
    fontSize: 18,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 15,
    padding: 5,
    margin: 5,
  },
  label: {
    marginLeft: 10,
    fontSize: 20,
    marginBottom: 5,
  },
});

export default AlbumDescEdit;
