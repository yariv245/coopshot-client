import createDataContext from "./createDataContext";
import axios from "../api/albums";

const globalReducer = (state, action) => {
  switch (action.type) {
    case "edit_defaut_album":
      state.defaultAlbumID = action.payload.albumID;
      state.defaultAlbumName = action.payload.albumName;
      return state;
    case "get_defaut_album":
      return state;
    case "get_favs_albums":
      state.favAlbums = action.payload;
      return state;
    case "edit_favs_albums":
      const albumID = action.payload.albumID;
      const favAlbum = action.payload.favorite;
      if (favAlbum) state.favAlbums = [...state.favAlbums, favAlbum];
      else
        state.favAlbums = state.favAlbums.filter(
          (album) => album._id !== albumID
        );
      return state;
    default:
      return state;
  }
};

const editDefaultAlbum = (dispatch) => {
  return async (albumID, albumName) => {
    dispatch({
      type: "edit_defaut_album",
      payload: { albumID: albumID, albumName: albumName },
    });
    // callback();
  };
};

const getDefaultAlbum = (dispatch) => {
  return async (callback) => {
    dispatch({
      type: "get_defaut_album",
      payload: {},
    });
    if (callback) callback();
  };
};

const getFavAlbums = (dispatch) => {
  return async () => {
    const response = await axios.get("/user/getAllFav").catch((err) => {
      console.error(err.message);
    });

    dispatch({ type: "get_favs_albums", payload: response.data });
  };
};

const updateFavAlbums = (dispatch) => {
  return async (albumID, callback) => {
    const response = await axios.post("/user/updateFav", {
      albumID,
    });
    dispatch({
      type: "edit_favs_albums",
      payload: { favorite: response.data, albumID },
    });
    if (callback) callback();
  };
};

export const { Context, Provider } = createDataContext(
  globalReducer,
  {
    editDefaultAlbum,
    getDefaultAlbum,
    getFavAlbums,
    updateFavAlbums,
  },
  {
    defaultAlbumID: "",
    defaultAlbumName: "",
    favAlbums: null,
  }
);
