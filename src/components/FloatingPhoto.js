/** @format */

import React, { useState, useEffect, useContext } from "react";
import { View, StyleSheet } from "react-native";
import { FAB, Portal, Provider } from "react-native-paper";
import { Feather, Ionicons, MaterialIcons } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import axios from "../api/albums";
import { Context as AlbumContext } from "../context/AlbumContext";
const FloatingPhoto = (props) => {
  const [state, setState] = React.useState({ open: false });
  const { deletePhotoAlbum } = useContext(AlbumContext);
  const onStateChange = ({ open }) => setState({ open });
  const { open } = state;

  return (
    <Provider style={styles.providerStyle}>
      <Portal>
        <FAB.Group
          color="black"
          open={open}
          icon={open ? "minus" : "plus"}
          actions={[
            {
              icon: () => (
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <MaterialCommunityIcons
                    name="card-account-details"
                    size={24}
                    color="black"
                  />
                </View>
              ),

              label: "Details",
              onPress: () => {
                props.nav.navigate("PhotoDetails", {
                  imageObj: props.image,
                  photosObjectAlbum: props.photosObjectAlbum,
                });
              },
              small: false,
            },
            {
              icon: (props) => (
                <MaterialIcons name="delete" size={24} color="black" />
              ),
              label: "Delete",
              onPress: () => {
                axios
                  .post("/api/deletephoto", {
                    public_id: props.image.public_id,
                    image_id: props.image._id,
                    albumID: props.image.album,
                  })
                  .then(() => {
                    deletePhotoAlbum(props.image.album, props.image._id);
                    props.nav.navigate("SingleAlbum", {
                      id: props.image.album,
                    });
                  });
              },
              small: false,
            },
          ]}
          onStateChange={onStateChange}
          onPress={() => {
            if (open) {
              // do something if the speed dial is open
            }
          }}
        />
      </Portal>
    </Provider>
  );
};

const styles = StyleSheet.create({
  providerStyle: {
    position: "absolute",
    bottom: 10,
    right: 10,
    marginBottom: 10,
    marginRight: 10,
  },
});

export default FloatingPhoto;
