import React, { useState } from 'react';
import LottieView from 'react-native-animated-loader';
import { Text, StyleSheet } from 'react-native';

const Loader = () => {
  return (
    <LottieView
      visible={true}
      overlayColor="rgba(255,255,255,0.75)"
      source={require('../../assets/loaderAkash.json')}
      animationStyle={styles.lottie}
      speed={1}
    >
      <Text>Doing something...</Text>
    </LottieView>
  );
};

const styles = StyleSheet.create({
  lottie: {
    width: 100,
    height: 100,
  },
});

export default Loader;
