import React, { useState } from "react";
import { SegmentedControlIOSComponent } from "react-native";
import { StyleSheet, View, Text } from "react-native";
import DynamicTabView from "react-native-dynamic-tab-view";
import RecommendedComp from "./RecommendedComp";

const onChangeTab = (index) => {};

const TabsComp = (props) => {
  const tagsPhotosRecommendedArray = props.tagsPhotosRecommendedArray;

  const [defaultIndex] = useState(0);

  const _renderItem = (item, index) => {
    return (
      <RecommendedComp
        tagsPhotosRecommendedArray={tagsPhotosRecommendedArray[index]}
      />
    );
  };

  const data1 = props.tagObjectsList.map((item,index) => {
    return { title: item.type, key: item._id, photosTagArray: item.photos };
  });

  return (
    <View>
      <DynamicTabView
        data={data1}
        renderTab={_renderItem}
        defaultIndex={defaultIndex}
        containerStyle={styles.container}
        headerBackgroundColor={"#003f5c"}
        headerTextStyle={styles.headerText}
        onChangeTab={onChangeTab}
        headerUnderlayColor={"white"}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  // `headerContainer: {
  //   marginTop: 16
  // },`
  headerText: {
    color: "white",
  },
  // tabItemContainer: {
  //   backgroundColor: "#cf6bab"
  // }
});

export default TabsComp;
